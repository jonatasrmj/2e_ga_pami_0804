import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalheSeriePage } from './detalhe-serie.page';

const routes: Routes = [
  {
    path: '',
    component: DetalheSeriePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalheSeriePageRoutingModule {}
