import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalheSeriePageRoutingModule } from './detalhe-serie-routing.module';

import { DetalheSeriePage } from './detalhe-serie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalheSeriePageRoutingModule
  ],
  declarations: [DetalheSeriePage]
})
export class DetalheSeriePageModule {}
